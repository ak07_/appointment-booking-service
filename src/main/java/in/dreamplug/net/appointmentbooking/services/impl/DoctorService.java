package in.dreamplug.net.appointmentbooking.services.impl;

import in.dreamplug.net.appointmentbooking.entry.DoctorEntry;
import in.dreamplug.net.appointmentbooking.repository.DoctorDAO;
import in.dreamplug.net.appointmentbooking.services.IDoctorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class DoctorService implements IDoctorService {

    @Autowired
    private DoctorDAO doctorDAO;

    @Override
    public DoctorEntry create(DoctorEntry doctorEntry) {
        return doctorDAO.save(doctorEntry);
    }

    @Override
    public Optional<DoctorEntry> findByDoctorId(Long doctorId) { return doctorDAO.findById(doctorId); }
}
