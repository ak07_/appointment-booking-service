package in.dreamplug.net.appointmentbooking.services.impl;

import in.dreamplug.net.appointmentbooking.entry.BookingEntry;
import in.dreamplug.net.appointmentbooking.entry.PatientEntry;
import in.dreamplug.net.appointmentbooking.repository.BookingDAO;
import in.dreamplug.net.appointmentbooking.repository.PatientDAO;
import in.dreamplug.net.appointmentbooking.services.IBookingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;

@Service
public class BookingService implements IBookingService {

    @Autowired
    private BookingDAO bookingDAO;

    @Autowired
    private PatientDAO patientDAO;

    @Override
    public BookingEntry create(BookingEntry bookingEntry) {
        return bookingDAO.save(bookingEntry);
    }

    @Override
    public Optional<BookingEntry> findByBookingId(Long bookingId) {
        return bookingDAO.findById(bookingId);
    }

    @Override
    public Optional<BookingEntry> cancelBookingById(Long bookingId) {
        Optional<BookingEntry> bookingEntry = bookingDAO.findById(bookingId);
        if(!bookingEntry.isPresent()) {
            return bookingEntry;
        }
        bookingDAO.deleteById(bookingId);
        return bookingEntry;
    }

    @Override
    public Boolean checkSlotOverlap(BookingEntry bookingEntry) {
        List<BookingEntry> bookingEntries = bookingDAO.findAllBookingByDate(bookingEntry.getDateOfAppointment());
        AtomicInteger flag = new AtomicInteger(0);
        bookingEntries.forEach(entry -> {
            LocalTime _startTime = entry.getStartTime().compareTo(bookingEntry.getStartTime())>0 ? entry.getStartTime() : bookingEntry.getStartTime();
            LocalTime _endTime = entry.getEndTime().compareTo(bookingEntry.getEndTime())>0 ? bookingEntry.getEndTime() : entry.getEndTime();
            if(_endTime.compareTo(_startTime) >= 0) {
                flag.set(1);
            }
        });
        if(flag.get() == 1) {
            return true;
        }
        return false;
    }

    @Override
    public BookingEntry rescheduleBookingById(Long bookingId, LocalDate date, LocalTime startTime, LocalTime endTime) {
        BookingEntry bookingEntry = bookingDAO.findById(bookingId).get();
        if(bookingEntry.getDateOfAppointment().compareTo(LocalDate.now())>0 ||
                (bookingEntry.getDateOfAppointment().compareTo(LocalDate.now())==0 && LocalTime.now().until(bookingEntry.getStartTime(), ChronoUnit.HOURS) >= 4)) {
            bookingEntry.setDateOfAppointment(date);
            bookingEntry.setStartTime(startTime);
            bookingEntry.setEndTime(endTime);
            bookingEntry.setUpdatedAt(LocalDateTime.now());
            return bookingDAO.save(bookingEntry);
        }
        return null;
    }

    @Override
    public List<PatientEntry> findAllPatientByDate(LocalDate date) {
        return patientDAO.findAllPatientsByDate(date);
    }
}
