package in.dreamplug.net.appointmentbooking.services.impl;

import in.dreamplug.net.appointmentbooking.entry.PatientEntry;
import in.dreamplug.net.appointmentbooking.repository.PatientDAO;
import in.dreamplug.net.appointmentbooking.services.IPatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class PatientService implements IPatientService {

    @Autowired
    private PatientDAO patientDAO;

    @Override
    public PatientEntry create(PatientEntry patientEntry) {
        return patientDAO.save(patientEntry);
    }

    @Override
    public Optional<PatientEntry> findByPatientId(Long patientId) {
        return patientDAO.findById(patientId);
    }

    @Override
    public Optional<PatientEntry> deletePatientById(Long patientId) {
        Optional<PatientEntry> patientEntry = patientDAO.findById(patientId);
        if(!patientEntry.isPresent()) {
            return patientEntry;
        }
        patientDAO.deleteById(patientId);
        return patientEntry;
    }
}
