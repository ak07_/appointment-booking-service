package in.dreamplug.net.appointmentbooking.services;

import in.dreamplug.net.appointmentbooking.entry.DoctorEntry;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.Optional;

@Validated
public interface IDoctorService {
    DoctorEntry create(@Valid DoctorEntry doctorEntry);
    Optional<DoctorEntry> findByDoctorId(@NotNull @Min(1) Long doctorId);
}
