package in.dreamplug.net.appointmentbooking.services;

import in.dreamplug.net.appointmentbooking.entry.PatientEntry;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.Optional;

@Validated
public interface IPatientService {
    PatientEntry create(@Valid PatientEntry patientEntry);
    Optional<PatientEntry> findByPatientId(@NotNull @Min(1) Long patientId);
    Optional<PatientEntry> deletePatientById(@NotNull @Min(1) Long patientId);
}
