package in.dreamplug.net.appointmentbooking.services;

import in.dreamplug.net.appointmentbooking.entry.BookingEntry;
import in.dreamplug.net.appointmentbooking.entry.PatientEntry;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;
import java.util.Optional;

@Validated
public interface IBookingService {
    BookingEntry create(@Valid BookingEntry bookingEntry);
    Boolean checkSlotOverlap(BookingEntry bookingEntry);
    Optional<BookingEntry> findByBookingId(@NotNull @Min(1) Long bookingId);
    Optional<BookingEntry> cancelBookingById(@NotNull @Min(1) Long bookingId);
    BookingEntry rescheduleBookingById(@NotNull @Min(1) Long bookingId, @NotNull LocalDate date, @NotNull LocalTime startTime, @NotNull LocalTime endTime);
    List<PatientEntry> findAllPatientByDate(@NotNull LocalDate date);
}
