package in.dreamplug.net.appointmentbooking;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication(scanBasePackages = {"in.dreamplug.net.appointmentbooking"})
@EnableJpaRepositories(basePackages = "in.dreamplug.net.appointmentbooking.repository")
public class AppointmentbookingApplication {

	public static void main(String[] args) {
		SpringApplication.run(AppointmentbookingApplication.class, args);
	}

}