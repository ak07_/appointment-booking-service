package in.dreamplug.net.appointmentbooking.controllers;

import in.dreamplug.net.appointmentbooking.entry.DoctorEntry;
import in.dreamplug.net.appointmentbooking.services.IDoctorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import java.util.Optional;

@RestController
@RequestMapping("/doctor")
public class DoctorController {

    @Autowired
    private IDoctorService iDoctorService;

    @GetMapping(path = "/get/{doctorId}",
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity get(@NotEmpty @PathVariable("doctorId") Long doctorId) {
        Optional<DoctorEntry> doctorEntry = iDoctorService.findByDoctorId(doctorId);
        if(doctorEntry.isPresent()){
            return ResponseEntity
                    .status(HttpStatus.OK)
                    .body(doctorEntry.get());
        }
        else{
            return ResponseEntity
                    .status(HttpStatus.NOT_FOUND)
                    .build();
        }
    }

    @PostMapping(path = "/",
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity create(@Valid @RequestBody DoctorEntry doctorEntry) {
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(iDoctorService.create(doctorEntry));
    }
}
