package in.dreamplug.net.appointmentbooking.controllers;

import in.dreamplug.net.appointmentbooking.entry.PatientEntry;
import in.dreamplug.net.appointmentbooking.services.IPatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import java.util.Optional;

@RestController
@RequestMapping("/patient")
public class PatientController {

    @Autowired
    private IPatientService iPatientService;

    @GetMapping(path = "/get/{patientId}",
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity get(@NotEmpty @PathVariable("patientId") Long patientId) {
        Optional<PatientEntry> patientEntry = iPatientService.findByPatientId(patientId);
        if(patientEntry.isPresent()){
            return ResponseEntity
                    .status(HttpStatus.OK)
                    .body(patientEntry.get());
        }
        else{
            return ResponseEntity
                    .status(HttpStatus.NOT_FOUND)
                    .build();
        }
    }

    @PostMapping(path = "/",
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity create(@Valid @RequestBody PatientEntry patientEntry) {
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(iPatientService.create(patientEntry));
    }

    @DeleteMapping(path = "/delete/{patientId}",
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity delete(@NotEmpty @PathVariable("patientId") Long patientId) {
        Optional<PatientEntry> patientEntry = iPatientService.deletePatientById(patientId);
        if(patientEntry.isPresent()){
            return ResponseEntity
                    .status(HttpStatus.OK)
                    .body(patientEntry.get());
        }
        else{
            return ResponseEntity
                    .status(HttpStatus.NOT_FOUND)
                    .build();
        }
    }
}
