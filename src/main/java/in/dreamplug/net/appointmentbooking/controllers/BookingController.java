package in.dreamplug.net.appointmentbooking.controllers;

import in.dreamplug.net.appointmentbooking.entry.BookingEntry;
import in.dreamplug.net.appointmentbooking.entry.DoctorEntry;
import in.dreamplug.net.appointmentbooking.entry.PatientEntry;
import in.dreamplug.net.appointmentbooking.services.IBookingService;
import in.dreamplug.net.appointmentbooking.services.IDoctorService;
import in.dreamplug.net.appointmentbooking.services.IPatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeParseException;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("/booking")
public class BookingController {

    @Autowired
    private IBookingService iBookingService;

    @Autowired
    private IPatientService iPatientService;

    @Autowired
    private IDoctorService iDoctorService;

    @GetMapping(path = "/get/{bookingId}",
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity get(@NotEmpty @PathVariable("bookingId") Long bookingId) {
        Optional<BookingEntry> bookingEntry = iBookingService.findByBookingId(bookingId);
        if(bookingEntry.isPresent()){
            return ResponseEntity
                    .status(HttpStatus.OK)
                    .body(bookingEntry.get());
        }
        else{
            return ResponseEntity
                    .status(HttpStatus.NOT_FOUND)
                    .build();
        }
    }

    @PostMapping(path = "/",
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity create(@Valid @RequestBody BookingEntry bookingEntry) {
        Optional<DoctorEntry> doctorEntry = iDoctorService.findByDoctorId(bookingEntry.getDoctorEntry().getId());
        Optional<PatientEntry> patientEntry = iPatientService.findByPatientId(bookingEntry.getPatientEntry().getId());
        if(!doctorEntry.isPresent() || !patientEntry.isPresent()) {
            return ResponseEntity
                    .status(HttpStatus.NOT_FOUND)
                    .build();
        }
        if(iBookingService.checkSlotOverlap(bookingEntry)) {
            return ResponseEntity
                    .status(HttpStatus.NOT_FOUND)
                    .body("Slot not available");
        }
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(iBookingService.create(bookingEntry));
    }

    @DeleteMapping(path = "/delete/{bookingId}",
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity delete(@NotEmpty @PathVariable("bookingId") Long bookingId) {
        Optional<BookingEntry> bookingEntry = iBookingService.cancelBookingById(bookingId);
        if(bookingEntry.isPresent()){
            return ResponseEntity
                    .status(HttpStatus.OK)
                    .body(bookingEntry.get());
        }
        else{
            return ResponseEntity
                    .status(HttpStatus.NOT_FOUND)
                    .build();
        }
    }

    @GetMapping(path = "/getpatients/{date}",
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity getPatients(@NotEmpty @PathVariable("date") String date) {
        LocalDate _date;
        try {
            _date = LocalDate.parse(date);
        }
        catch (DateTimeParseException ex) {
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .build();
        }
        List<PatientEntry> patientEntries = iBookingService.findAllPatientByDate(_date);
        if(patientEntries.size() > 0){
            return ResponseEntity
                    .status(HttpStatus.OK)
                    .body(patientEntries);
        }
        else{
            return ResponseEntity
                    .status(HttpStatus.NOT_FOUND)
                    .build();
        }
    }

    @PatchMapping(path = "/reschedule/{bookingId}",
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity reschedule(@NotEmpty @PathVariable("bookingId") Long bookingId, @Valid @RequestBody Map<String, String> requestBody) {
        if(!(requestBody.containsKey("date_of_appointment") && requestBody.containsKey("start_time") && requestBody.containsKey("end_time"))) {
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .build();
        }
        LocalDate _date;
        LocalTime _startTime;
        LocalTime _endTime;
        try {
            _date = LocalDate.parse(requestBody.get("date_of_appointment"));
            _startTime = LocalTime.parse(requestBody.get("start_time"));
            _endTime = LocalTime.parse(requestBody.get("end_time"));
        }
        catch (DateTimeParseException ex) {
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .build();
        }
        BookingEntry bookingEntry = iBookingService.rescheduleBookingById(bookingId, _date, _startTime, _endTime);
        if(bookingEntry != null) {
            return ResponseEntity
                    .status(HttpStatus.OK)
                    .body(bookingEntry);
        }
        else {
            return ResponseEntity
                    .status(HttpStatus.NOT_FOUND)
                    .body("Can't reschedule less than 4hrs before the appointment");
        }
    }
}
