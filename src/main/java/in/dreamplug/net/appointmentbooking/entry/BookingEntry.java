package in.dreamplug.net.appointmentbooking.entry;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import javax.validation.constraints.NotNull;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalTime;

@Data
@Entity
@Table(name = "bookings")
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
@EqualsAndHashCode(callSuper = true)
public class BookingEntry extends BaseEntry{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    private LocalDate dateOfAppointment;

    @NotNull
    private LocalTime startTime;

    @NotNull
    private LocalTime endTime;

    @ManyToOne(optional = false)
    @JoinColumn(name = "doctor_entry_id", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private DoctorEntry doctorEntry;

    @ManyToOne(optional = false)
    @JoinColumn(name = "patient_entry_id", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private PatientEntry patientEntry;
}
