package in.dreamplug.net.appointmentbooking.entry;

import io.micrometer.core.instrument.util.StringUtils;
import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@MappedSuperclass
public abstract class BaseEntry implements Serializable {
    @CreatedDate
    @Column(nullable = false, updatable = false)
    private LocalDateTime createdAt;

    //@Value("system")
    @CreatedBy
    @Column(nullable = false, updatable = false)
    private String createdBy;

    @LastModifiedDate
    @Column(nullable = false)
    private LocalDateTime updatedAt;

    //@Value("system")
    @LastModifiedBy
    @Column(nullable = false)
    private String lastModifiedBy;

    @PrePersist
    public void onPrePersist() {
        LocalDateTime date = LocalDateTime.now();
        this.updatedAt = date;
        this.createdAt = date;
        if(StringUtils.isBlank(this.createdBy)) {
            this.createdBy = "system";
            this.lastModifiedBy = "system";
        }
    }
}
