package in.dreamplug.net.appointmentbooking.repository;

import in.dreamplug.net.appointmentbooking.entry.DoctorEntry;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DoctorDAO extends JpaRepository<DoctorEntry, Long> {
}
