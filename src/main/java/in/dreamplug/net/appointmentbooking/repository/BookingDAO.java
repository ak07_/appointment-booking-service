package in.dreamplug.net.appointmentbooking.repository;

import in.dreamplug.net.appointmentbooking.entry.BookingEntry;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface BookingDAO extends JpaRepository<BookingEntry, Long> {

    @Query(value = "SELECT * FROM BOOKINGS WHERE DATE_OF_APPOINTMENT=:date", nativeQuery = true)
    List<BookingEntry> findAllBookingByDate(@Param("date") LocalDate date);

}
