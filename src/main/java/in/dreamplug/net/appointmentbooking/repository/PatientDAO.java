package in.dreamplug.net.appointmentbooking.repository;

import in.dreamplug.net.appointmentbooking.entry.PatientEntry;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface PatientDAO extends JpaRepository<PatientEntry, Long> {

    @Query(value = "SELECT * FROM PATIENTS WHERE ID IN (SELECT PATIENT_ENTRY_ID FROM BOOKINGS WHERE DATE_OF_APPOINTMENT=:date)", nativeQuery = true)
    List<PatientEntry> findAllPatientsByDate(@Param("date") LocalDate date);
}
